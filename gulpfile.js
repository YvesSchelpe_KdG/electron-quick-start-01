'use strict';

const logprefix = (taskName) => {
    const time = new Date();
    let prefix = ` ## [${time.getHours()}:${(time.getMinutes() < 10 ? '0' : '')}${time.getMinutes()}:${time.getSeconds()}]-[GULP]`;
    if (taskName !== undefined) {
        prefix = `${prefix}-[${taskName}]`;
    }
    return prefix;
};

const { argv } = require('yargs');
function getEnvName() {
    return argv.env || 'development';
}

const gulp = require('gulp');
gulp.task('start', ['environment', 'package.json', 'bower'], () => {
    console.log(`${logprefix('start')} Starting in '${getEnvName()}-environment'`);
});

const jetpack = require('fs-jetpack');
const projectDirectory = jetpack;
const srcDirectory = projectDirectory.cwd('./src');
// detect environment and copy corresponding file to ./src/boot/env.json
gulp.task('environment', () => {
    console.log(`${logprefix('environment')} Building env.json for '${getEnvName()}-environment'`);
    const envSourceFile = `${projectDirectory.path(`config/env_${getEnvName()}.json`)}`;
    const envTargetFile = `${srcDirectory.path('boot/env.json')}`;
    console.log(`${logprefix('environment')} Will copy '${envSourceFile}' to '${envTargetFile}'`);
    jetpack.copy(envSourceFile, envTargetFile, { overwrite: true });
});

// copy package.json root, delete devDependencies & change "main" and write to ./src/package.json
gulp.task('package.json', () => {
    console.log(`${logprefix('package.json')} Building package.json for src folder '${srcDirectory.path()}'.`);
    const envSourceFile = `${projectDirectory.path(`package.json`)}`;
    let envSourceContent = jetpack.read(envSourceFile, 'json');
    envSourceContent.main = "main.js";
    envSourceContent.scripts.start = "electron .";
    delete envSourceContent.scripts.release;
    delete envSourceContent.scripts.prerelease;
    console.log(`${logprefix('package.json')} "main" property set to '${envSourceContent.main}'.`);
    delete envSourceContent.devDependencies;
    console.log(`${logprefix('package.json')} "devDependencies" property set to '${envSourceContent.devDependencies}'.`);
    const envTargetFile = srcDirectory;
    console.log(`${logprefix('package.json')} Writing changes to '${envTargetFile}'.`);
    envTargetFile.write('package.json', envSourceContent, { overwrite: true });
});

// bower dependencies
const bowerDirectory = projectDirectory.cwd('./bower_components');
gulp.task('bower', ['jquery', 'fontawesome'], () => {
    console.log(`${logprefix('bower')} Taking care of bower dependencies targetting the '${getEnvName()}-environment'.`);
});
gulp.task('jquery', () => {
    console.log(`${logprefix('bower-jquery')} Taking care jquery components.`);
    const sourceDirectory = bowerDirectory.cwd('./jquery/dist');
    const targetDirectory = srcDirectory.cwd('./lib');
    sourceDirectory.copy('.', targetDirectory.path(), {
        matching: ['*.min.js'],
        overwrite: true
    });
});
gulp.task('fontawesome', () => {
    console.log(`${logprefix('bower-fontawesome')} Taking care fontawesome components.`);
    const sourceDirectoryCss = bowerDirectory.cwd('./font-awesome/css');
    const sourceDirectoryFonts = bowerDirectory.cwd('./font-awesome/fonts');
    const targetDirectoryCss = srcDirectory.cwd('./styles');
    const targetDirectoryFonts = srcDirectory.cwd('./fonts');
    sourceDirectoryCss.copy('.', targetDirectoryCss.path(), {
        matching: ['*.min.css', '!*.slim.min.js'],
        overwrite: true
    });
    sourceDirectoryFonts.copy('.', targetDirectoryFonts.path(), {
        matching: ['*.otf', '*.eot', '*.svg', '*.ttf', '*.woff', '*.woff2'],
        overwrite: true
    });
});