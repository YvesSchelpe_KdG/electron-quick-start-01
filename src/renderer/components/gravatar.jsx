'use strict';

import React, { Component } from 'react';
import md5 from 'md5';
const GRAVATAR_URL = "http://gravatar.com/avatar";

export class Gravatar extends Component {
    constructor() {
        super();
    }
    static get propTypes() {
        return {
            user: React.PropTypes.shape({
                name: React.PropTypes.string,
                email: React.PropTypes.string
            }).isRequired
        };
    }
    render() {
        const size = 36 * 2;
        const hash = md5(this.props.user.email);
        const url = `${GRAVATAR_URL}/${hash}?s=${size}`;
        return (
            <div id='gravatar'>
                <img id='gravatarImage' className="gravatar" src={url} width={size} />
                <div className="info">
                    <p id='gravatarName'>{this.props.user.name}</p>
                    <p id='gravatarEmail'><a href={`mailto:${this.props.user.email}`}> {this.props.user.email}</a></p>
                </div>
            </div>
        );
    }
}