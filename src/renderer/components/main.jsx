'use strict';

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { shell } from 'electron';
import env from '../../boot/env';
import { Gravatar } from './gravatar';

export class Main extends Component {
  constructor(props) {
    super(props);
    this.state = { message: 'Hello, Electron', environment: env.name };
  }
  openGithub() {
    shell.openExternal('https://github.com/Quramy/electron-jsx-babel-boilerplate');
  }
  openGitLab() {
    shell.openExternal('https://gitlab.com/YvesSchelpe_KdG/electron-quick-start-01');
  }
  render() {
    const users = [
      { "email": "ys@delegate-it.be", "name": "Yves Schelpe (Delegate-IT.be)" },
      { "email": "yves.schelpe@kdg.be", "name": "Yves Schelpe (KdG)" },
      { "email": "yves.schelpe@psyaviah.com", "name": "Yves Schelpe (Psy'Aviah)" },
      { "email": "yves.schelpe@gmail.com", "name": "Yves Schelpe (Prive)" }
    ].map(user => {
      return <Gravatar key={user.email} user={user} />
    });

    return (
      <div className="container">
        <div className="jumbotron main">
          <h1>{this.state.message}</h1>
          <img className="svg" src="../assets/images/electron.svg" alt="" width="128px"></img>
          <p>Running in '{this.state.environment}'-mode. Provided by <a href="#" onClick={this.openGitLab}>electron-quick-start-01 on GitLab</a> and originally based on by <a href="#" onClick={this.openGithub}>electron-jsx-babel-boilerplate</a>.</p>
          <div id='gravatars'>
            {users}
          </div>
        </div>
      </div>
    );
  }
}