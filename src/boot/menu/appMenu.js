'use strict';

import { app, Menu, MenuItem } from 'electron';

let template = [{
    label: 'Electron App',
    submenu: [{
        label: 'Quit',
        accelerator: 'Command+Q',
        click: function () { app.quit(); }
    }]
}];

let appMenu = Menu.buildFromTemplate(template);

module.exports = appMenu;