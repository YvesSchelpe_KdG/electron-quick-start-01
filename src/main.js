'use strict';

import { app, BrowserWindow, crashReporter, Menu } from 'electron';
import appMenu from './boot/menu/appMenu';
import env from './boot/env';

let mainWindow = null;
if (env.name === 'development') {
    require('electron-reload')(__dirname);
}

app.on('window-all-closed', () => {
    console.log(`${logprefix('app:event:window-all-closed')} Calling app.quit().`);
    app.quit();
});

app.on('ready', () => {
    console.log(`${logprefix('app:event:ready')} Running in '${env.name}-environment'.`);
    mainWindow = new BrowserWindow({
        width: 580,
        height: 365
    });

    if (env.name === 'production') {
        mainWindow.setMenu(null);
    }
    if (env.name === 'development') {
        mainWindow.openDevTools();
    }

    mainWindow.loadURL(`file://${__dirname}/renderer/index.html`);
});

const logprefix = (taskName) => {
    const time = new Date();
    let prefix = ` ## [${time.getHours()}:${(time.getMinutes() < 10 ? '0' : '')}${time.getMinutes()}:${time.getSeconds()}]-[ELECTRON]`;
    if (taskName !== undefined) {
        prefix = `${prefix}-[${taskName}]`;
    }
    return prefix;
};